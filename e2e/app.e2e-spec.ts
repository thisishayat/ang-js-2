import { AgntwoPage } from './app.po';

describe('agntwo App', function() {
  let page: AgntwoPage;

  beforeEach(() => {
    page = new AgntwoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
